use deflate::deflate_bytes;
use inflate::inflate_bytes;
use std::fs::File;
use std::io::Write;
use std::fs::read;

fn main()
{
    // reading test file

    let in_path = "test.wav";
    let mut file_data = Vec::<u8>::new();
    file_data = read(in_path).unwrap();
    println!("Read {} bytes!", file_data.len());

    let compressed_data = deflate::deflate_bytes(&file_data);
    println!("Compressed: {} bytes", compressed_data.len());

    let out_path = "out.dat";
    let mut f = File::create(out_path).expect("Error creating file");
    f.write_all(&compressed_data).expect("Error writing file");
    println!("File was written");

    let decomp_path = "out.dat";    
    let decompressed = read(decomp_path).unwrap();

    let final_data = inflate::inflate_bytes(&decompressed).unwrap();

    let mut outfile = File::create("decompressed.wav").expect("Error create");
    outfile.write_all(&final_data).expect("Error write");
    println!("File written!");
}
